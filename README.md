# Ecerto tools
Unofficial userscript for Ecerto Web Platform (EM-Group).

## Install
https://regapictures-userscripts.gitlab.io/ecerto-tools/bundle.user.js

## Download
https://gitlab.com/regapictures-userscripts/ecerto-tools/-/jobs/artifacts/master/raw/dist/bundle.user.js?job=build