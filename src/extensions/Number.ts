interface Number {
    zeroPrefix(minLength?: number): string;
}

Number.prototype.zeroPrefix = function (minLength: number = 2) {
    const numberStr = this.toString();
    return numberStr.length < minLength ? '0'.repeat(minLength - numberStr.length) + numberStr : numberStr;
}