import { Time } from "./models/Time";
import { Registrations } from "./models/Registrations";

module.require('./extensions/Number');

function calculateStopTime(toWorkDuration: string, registrationTimes: string[]): Time | undefined {
    return Registrations
        .fromStrings(registrationTimes)
        .getNeutralStopTimeFor(Time.fromHHMMString(toWorkDuration));
}

function getRegistrationTimes(registrationTable: HTMLTableElement): string[] {
    const times: string[] = [];
    const timeStrings = registrationTable.querySelectorAll('tr:not(.datagridheader):not(:first-child) > td:nth-child(2) > span');
    for (let i = 0; i < timeStrings.length; i++) {
        times.push(timeStrings[i].innerHTML);
    }

    return times;
}

function getToWorkTime(): string {
    const toWorkDurationElement = document.querySelector('#registraties_lblEffectiefTePresteren');
    if (toWorkDurationElement == null) {
        const defaultDuration = '7u48';
        console.warn('HTMLElement for work duration not found! Using default duration ' + defaultDuration + '.');
        return defaultDuration;
    } else {
        return toWorkDurationElement.innerHTML;
    }
}

function createStopTimeTableRow(stopTime: Time) {
    return '<tr>'
        + '<td></td>'
        + '<td>'
        + '<span class="label" style="font-style: italic">'
        + stopTime.toString()
        + '</span>'
        + '</td>'
        + '<td></td>'
        + '<td>Neutrale stoptijd</td>'
        + '</tr>';
}

function run() {
    const registrationTable = <HTMLTableElement>document.querySelector('table#registraties_dgrRegistraties');
    if (registrationTable) {
        const times = getRegistrationTimes(registrationTable);
        const toWorkDuration = getToWorkTime();
        const stopTime = calculateStopTime(toWorkDuration, times);
        if (stopTime) {
            registrationTable.innerHTML += createStopTimeTableRow(stopTime);
        }
    }
}

run();