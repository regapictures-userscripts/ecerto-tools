import { Time } from "./Time";

export class Registrations {
    private registrations: Time[];

    constructor(registrations: Time[] = []) {
        this.registrations = registrations;
    }

    public static fromStrings(registrationStrings: string[]): Registrations {
        const registrations: Time[] = [];
        for (const registrationString of registrationStrings) {
            registrations.push(Time.fromHHMMString(registrationString));
        }

        return new Registrations(registrations);
    }

    public getTimeWorked() {
        let totalTime = new Time();
        for (let i = 0; i + 1 < this.registrations.length; i += 2) {
            totalTime = totalTime.plus(this.registrations[i + 1].minus(this.registrations[i]));
        }

        return totalTime;
    }

    public getRemainingWorkTimeFor(workDuration: Time): Time {
        return workDuration.minus(this.getTimeWorked());
    }

    public getNeutralStopTimeFor(workDuration: Time): Time | undefined {
        if (this.registrations.length === 0) {
            return undefined;
        } else {
            const lastRegistration = this.registrations[this.registrations.length - 1];
            const remainingWorkTime = this.getRemainingWorkTimeFor(workDuration);
            return lastRegistration.plus(remainingWorkTime);
        }
    }
}