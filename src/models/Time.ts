export class Time {
    private minutes: number;

    constructor(minutes: number = 0) {
        this.minutes = minutes;
    }

    static now() {
        const date = new Date();
        return new Time(date.getHours() * 60 + date.getMinutes());
    }

    static fromHHMMString(hhmm: string) {
        const splittedHhmm = Time.splitString(hhmm);
        const hours = Number(splittedHhmm[0]);
        const minutes = Number(splittedHhmm[1]);
        Time.checkNotNan(splittedHhmm, hours, minutes);

        return new Time(hours * 60 + minutes);
    }

    private static checkNotNan(splittedHhmm: string[], hours: number, minutes: number) {
        if (isNaN(hours)) {
            throw new Error('hours is not a number, value: ' + splittedHhmm[0]);
        }
        if (isNaN(minutes)) {
            throw new Error('hours is not a number, value: ' + splittedHhmm[1]);
        }
    }

    private static splitString(hhmm: string) {
        if (hhmm.indexOf(':') !== -1) {
            return hhmm.split(":");
        } else if (hhmm.indexOf('u') !== -1) {
            return hhmm.split("u");
        } else {
            throw new Error('Unknown HHMM format');
        }
    }

    plus(delta: Time) {
        return new Time(this.minutes + delta.minutes);
    }

    minus(delta: Time) {
        return new Time(this.minutes - delta.minutes);
    }

    toString() {
        const hours = Math.floor(this.minutes / 60);
        const minutes = this.minutes - hours * 60;
        return hours + ':' + minutes.zeroPrefix();
    }
}